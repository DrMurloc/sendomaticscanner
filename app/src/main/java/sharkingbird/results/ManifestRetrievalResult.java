package sharkingbird.results;

import sharkingbird.entities.Ticket;

/**
 * Created by jonec on 3/18/2018.
 */

public class ManifestRetrievalResult {
    public final class Success extends ManifestRetrievalResult
    {
        public Ticket[] GetTickets()
        {
            return _tickets;
        }
        private Ticket[] _tickets;
        public Success(Ticket[] tickets)
        {
            _tickets = tickets;
        }
    }
    public final class Error extends ManifestRetrievalResult
    {
        public Exception GetException()
        {
            return _exception;
        }
        private Exception _exception;
        public Error(Exception exception)
        {
            _exception = exception;
        }
    }
}
