package sharkingbird.sendomaticscanner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import sharkingbird.datalayer.ManifestDatabase;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class ManifestSelector extends AppCompatActivity {
    private AppCompatActivity _me;
    private Spinner _nameSpinner;
    private Button _newManifestButton;
    private Button _viewManifestButton;
    private Button _scanTicketButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _me=this;
        setContentView(R.layout.activity_manifest_selector);
        _nameSpinner = findViewById(R.id.manifests_spinner);
        _newManifestButton = findViewById(R.id.new_manifest_button);
        _viewManifestButton = findViewById(R.id.view_manifest_button);
        _scanTicketButton = findViewById(R.id.scan_button);
        _newManifestButton.setOnClickListener(_newManifestClick);
        _viewManifestButton.setOnClickListener(_viewManifestClick);
        _scanTicketButton.setOnClickListener(_scanTicketClick);
        FillManifestNames();
    }
    private void FillManifestNames()
    {
        ManifestDatabase database = new ManifestDatabase(getApplicationContext());
        String[] names = database.GetManifestNames();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,names);
        _nameSpinner.setAdapter(adapter);
        if(names.length==0)
        {
            _nameSpinner.setVisibility(INVISIBLE);
            _viewManifestButton.setVisibility(INVISIBLE);
            _scanTicketButton.setVisibility(INVISIBLE);
        }
        else
        {
            _nameSpinner.setVisibility(VISIBLE);
            _viewManifestButton.setVisibility(VISIBLE);
            _scanTicketButton.setVisibility(VISIBLE);

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode)
        {
            case ActivityResults.SCAN_TICKET:
                if(data!=null)
                {
                }
                break;
            case ActivityResults.ADD_MANIFEST:
                FillManifestNames();
                break;
            case ActivityResults.VIEW_MANIFEST:
                FillManifestNames();
                break;
        }
    }

    private View.OnClickListener _scanTicketClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent redirectToScannerIntent = new Intent(_me,BarcodeScanner.class);
            startActivityForResult(redirectToScannerIntent,ActivityResults.SCAN_TICKET);
        }
    };
    private View.OnClickListener _viewManifestClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            Intent redirectToManifestViewer = new Intent(_me, ManifestViewer.class);
            redirectToManifestViewer.putExtra("ManifestName",_nameSpinner.getSelectedItem().toString());
            startActivityForResult(redirectToManifestViewer, ActivityResults.VIEW_MANIFEST);
        }
    };
    private View.OnClickListener _newManifestClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            Intent redirectToManifestAdder = new Intent(_me, ManifestAdder.class);
            startActivityForResult(redirectToManifestAdder, ActivityResults.ADD_MANIFEST);
        }
    };
    class ActivityResults
    {
        private final static int SCAN_TICKET=0;
        private final static int ADD_MANIFEST=SCAN_TICKET+1;
        private final static int VIEW_MANIFEST=ADD_MANIFEST+1;
    }
}
