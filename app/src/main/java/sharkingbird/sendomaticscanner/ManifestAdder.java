package sharkingbird.sendomaticscanner;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.ParseException;

import sharkingbird.datalayer.ManifestDatabase;
import sharkingbird.datalayer.ManifestRetriever;
import sharkingbird.entities.Ticket;
import sharkingbird.results.ManifestRetrievalResult;

/**
 * Created by jonec on 4/7/2018.
 */

public class ManifestAdder extends AppCompatActivity {

    private Button _addButton;
    private EditText _nameTextBox;
    private EditText _idTextBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manifest_adder);
        _addButton = findViewById(R.id.add_button);
        _nameTextBox = findViewById(R.id.name_text_box);
        _idTextBox = findViewById(R.id.id_text_box);
        _addButton.setOnClickListener(_addButtonClick);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    private View.OnClickListener _addButtonClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view) {
            String name = _nameTextBox.getText().toString();
            if(name.trim().equals(""))
            {
                Toast.makeText(getApplicationContext(), "You must input a name",
                        Toast.LENGTH_LONG).show();
                return;
            }
            String idString = _idTextBox.getText().toString();
            if(idString.trim().equals(""))
            {
                Toast.makeText(getApplicationContext(), "You must input an ID",
                        Toast.LENGTH_LONG).show();
                return;
            }
            int id;
            try {
                id = Integer.parseInt(idString);
            } catch(NumberFormatException e) {
                Toast.makeText(getApplicationContext(), "ID must be a number",
                        Toast.LENGTH_LONG).show();
                return;
            }
            new RetrieveHtml().execute(new RetrieveHtmlParams(id,name));
        }
    };
    class RetrieveHtmlParams
    {
        public RetrieveHtmlParams(int id, String name)
        {
            Id=id;
            Name=name;
        }
        public int Id;
        public String Name;
    }
    class RetrieveHtml extends AsyncTask<RetrieveHtmlParams, Void, Void> {
        protected Void doInBackground(RetrieveHtmlParams... params) {
            ManifestRetriever retriever = new ManifestRetriever(params[0].Id);
            ManifestRetrievalResult result = retriever.GetTickets();
            if(result instanceof ManifestRetrievalResult.Success)
            {
                ManifestRetrievalResult.Success success = (ManifestRetrievalResult.Success) result;
                ManifestDatabase database = new ManifestDatabase(getApplicationContext());
                database.AddManifest(params[0].Name, params[0].Id, success.GetTickets());
                finish();
                return null;
            }
            Toast.makeText(getApplicationContext(), "There wsa an error retrieving the manifest",
                    Toast.LENGTH_LONG);
            return null;
        }

    }
    
}