package sharkingbird.sendomaticscanner;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import sharkingbird.datalayer.ManifestDatabase;
import sharkingbird.entities.Ticket;

/**
 * Created by jonec on 4/11/2018.
 */

public class ManifestViewer extends AppCompatActivity {
    private AppCompatActivity _me;
    private ManifestDatabase _database;
    private ListView _ticketListView;
    private Button _deleteButton;
    private ConstraintLayout _layout;
    private String _manifestName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _me=this;
        setContentView(R.layout.activity_manifest_viewer);
        _layout = findViewById(R.id.manifest_viewer_layout);
        _database = new ManifestDatabase(getApplicationContext());
        _ticketListView = findViewById(R.id.ticket_list);
        _deleteButton = findViewById(R.id.delete_manifest_button);
        _deleteButton.setOnClickListener(deleteClicked);
        _manifestName = getIntent().getStringExtra("ManifestName");
        Ticket[] tickets = _database.GetTickets(_manifestName);
        ArrayList<String> ticketStrings = new ArrayList<>();
        for(Ticket ticket : tickets)
        {
            ticketStrings.add(ticket.GetTicketNumber()+" - "+ticket.GetOrderedBy()+(ticket.GetChecked()?" (checked in)":""));
        }
        _ticketListView.setAdapter(new ArrayAdapter(this,android.R.layout.simple_list_item_1,ticketStrings));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
    private View.OnClickListener deleteClicked = new View.OnClickListener()
    {
        @Override
        public void onClick(View view) {

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.confirm_delete_popup,(ViewGroup)findViewById(R.id.delete_popup_layout));
            final PopupWindow pw = new PopupWindow(layout,_layout.getWidth()*4/5,300,true);
            pw.showAtLocation(layout, Gravity.CENTER,0,0);
            layout.setBackgroundColor(Color.WHITE);

            Button cancelButton = layout.findViewById(R.id.cancel_delete_button);
            Button confirmButton = layout.findViewById(R.id.confirm_delete_button);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pw.dismiss();
                }
            });
            confirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    _database.DeleteManifest(_manifestName);
                    _me.finish();
                }
            });
        }
    };
}