package sharkingbird.sendomaticscanner;
import android.*;
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.text.NumberFormat;
import java.util.Timer;
import java.util.TimerTask;

import sharkingbird.datalayer.ManifestDatabase;
import sharkingbird.entities.Ticket;
import sharkingbird.views.CameraPreview;

/**
 * Activity for the multi-tracker app.  This app detects barcodes and displays the value with the
 * rear facing camera. During detection overlay graphics are drawn to indicate the position,
 * size, and ID of each barcode.
 */
public final class BarcodeScanner extends AppCompatActivity {

    /**
     * Initializes the UI and creates the detector pipeline.
     */
    private Camera _camera;
    private CameraPreview _cameraPreview;
    private BarcodeDetector _barcodeDetector;
    private FrameLayout _frame;
    private ManifestDatabase _database;
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_barcode_scanner);

        _database = new ManifestDatabase(getApplicationContext());
        _frame = findViewById(R.id.camera_preview);

        _barcodeDetector = new BarcodeDetector.Builder(getApplicationContext())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if(rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA},ActivityRequest.CAMERA_PERMISSION_REQUEST);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == ActivityRequest.CAMERA_PERMISSION_REQUEST)
        {
            if(grantResults[0]==PackageManager.PERMISSION_GRANTED)
            {
                createCameraSource();
            } else {
                finish();
            }
        }
    }
    private void createCameraSource()
    {
        _camera = getCameraInstance();
        _camera.setDisplayOrientation(90);
        _cameraPreview = new CameraPreview(this,_camera);
        _cameraPreview.setDrawingCacheEnabled(true);
        _frame.addView(_cameraPreview);
        _frame.setOnClickListener(screenClicked);

    }
    private View.OnClickListener screenClicked = new View.OnClickListener()
    {
        @Override
        public void onClick(View view) {
            _camera.takePicture(null,null,_pictureCallback);
        }
    };
    private Camera.PictureCallback _pictureCallback = new Camera.PictureCallback()
    {

        @Override
        public void onPictureTaken(byte[] bytes, Camera camera) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
            Frame frame = new Frame.Builder().setBitmap(bitmap).build();
            SparseArray<Barcode> barcodes = _barcodeDetector.detect(frame);
            if(barcodes.size()>0)
            {
                String displayValue = barcodes.get(barcodes.keyAt(0)).displayValue;
                long ticketId;
                try
                {
                    ticketId = Long.parseLong(displayValue);
                } catch(NumberFormatException e)
                {
                    ticketId=Long.parseLong(displayValue.split("\n")[0].substring("TICKET_ID-".length()));
                }
                HandleTicket(ticketId);
            }
            camera.startPreview();
        }
    };
    private void HandleTicket(long ticketNum)
    {

        Ticket ticket = _database.GetTicket(ticketNum);
        if(ticket==null)
        {
            ShowPopup("Ticket number "+ticketNum+" was not found in the manifest.",false);
        }
        else if(ticket.GetChecked())
        {
            ShowPopup("Ticket number "+ticketNum+" for "+ticket.GetOrderedBy()+" was already checked in", false);
        }
        else
        {
            ShowPopup("Ticket number "+ticketNum+" for "+ticket.GetOrderedBy()+" checked in!",true);
            _database.CheckTicket(ticketNum);
        }

    }
    private void ShowPopup(String message, boolean success)
    {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.ticket_popup,(ViewGroup)findViewById(R.id.popup_layout));
        final PopupWindow pw = new PopupWindow(layout,_frame.getWidth()*4/5,150,true);
        pw.showAtLocation(layout, Gravity.CENTER,0,0);
        layout.setBackgroundColor(Color.BLACK);

        TextView messageText = layout.findViewById(R.id.popup_message);
        messageText.setTextColor(success? Color.GREEN:Color.RED);
        messageText.setText(message);
        _frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pw.dismiss();
                _frame.setOnClickListener(screenClicked);
            }
        });
    }
    private static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }
    private Bitmap getBitmap()
    {

        _cameraPreview.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
        _cameraPreview.setDrawingCacheEnabled(true);
        _cameraPreview.buildDrawingCache();

        final Bitmap bitmap = Bitmap.createBitmap(_cameraPreview.getDrawingCache());
        _cameraPreview.setDrawingCacheEnabled(false);
        _cameraPreview.destroyDrawingCache();
        return bitmap;
    }
    private class ActivityRequest {
        public final static int CAMERA_PERMISSION_REQUEST=0;
    }
}