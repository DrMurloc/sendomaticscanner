package sharkingbird.entities;

public class Ticket {
    public Ticket(long ticketNumber, String ticketType, double pricePaid, String orderedBy) {
        _ticketNumber = ticketNumber;
        _ticketType = ticketType;
        _pricePaid = pricePaid;
        _orderedBy = orderedBy;
    }

    public boolean GetChecked() {
        return _checked;
    }

    public void SetChecked(boolean checked) {
        _checked = checked;
    }

    private boolean _checked;

    public long GetTicketNumber() {
        return _ticketNumber;
    }

    private long _ticketNumber;

    public String GetTicketType() {
        return _ticketType;
    }

    private String _ticketType;

    public double GetPricePaid() {
        return _pricePaid;
    }

    private double _pricePaid;

    public String GetOrderedBy() {
        return _orderedBy;
    }

    private String _orderedBy;
}