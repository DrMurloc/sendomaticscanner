package sharkingbird.datalayer;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

import sharkingbird.entities.Ticket;

/**
 * Created by jonec on 4/7/2018.
 */

public class ManifestDatabase  extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ManifestDatabase.db";
    public ManifestDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("" +
                "CREATE TABLE Manifest (" +
                "ManifestId INTEGER," +
                "Name TEXT PRIMARY KEY);");
        db.execSQL("" +
                "CREATE TABLE Ticket (" +
                "TicketId INTEGER PRIMARY KEY," +
                "ManifestName INTEGER, " +
                "TicketType TEXT," +
                "PricePaid REAL," +
                "OrderedBy TEXT," +
                "Checked INTEGER);");
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Version one, there should be no upgrades yet.
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Version one, there are no downgrades yet.
        onUpgrade(db, oldVersion, newVersion);
    }
    public String[] GetManifestNames()
    {
        ArrayList<String> nameList = new ArrayList<String>();
        Cursor cursor = getReadableDatabase().rawQuery("" +
                "SELECT Name " +
                "FROM Manifest " +
                "ORDER BY ManifestId DESC", new String[]{});
        while(cursor.moveToNext())
        {
            nameList.add(cursor.getString(0));
        }
        return nameList.toArray(new String[0]);
    }
    public Ticket GetTicket(long ticketNum)
    {
        String sql = "" +
                "SELECT t.TicketId, t.TicketType, t.PricePaid, t.OrderedBy, t.Checked " +
                "FROM Ticket t " +
                "WHERE t.TicketId = "+ticketNum;
        Cursor cursor = getReadableDatabase().rawQuery(sql,null);
        if(cursor.moveToNext())
        {
            Ticket ticket = new Ticket(cursor.getLong(0),cursor.getString(1),cursor.getDouble(2),cursor.getString(3));
            ticket.SetChecked(cursor.getInt(4)==1);
            return ticket;
        }
        return null;
    }
    public void CheckTicket(long ticketNum)
    {
        String sql ="" +
                "UPDATE Ticket " +
                "SET Checked = 1 " +
                "WHERE TicketId = "+ticketNum;
        getWritableDatabase().execSQL(sql);
    }
    public Ticket[] GetTickets(String manifestName)
    {
        String sql = "" +
                "SELECT t.TicketId, t.TicketType, t.PricePaid, t.OrderedBy, t.Checked " +
                "FROM Ticket t " +
                "WHERE t.ManifestName = ?";
        ArrayList<Ticket> ticketList = new ArrayList<Ticket>();
        Cursor cursor = getReadableDatabase().rawQuery(sql,new String[]{manifestName});
        while(cursor.moveToNext())
        {
            Ticket newTicket = new Ticket(cursor.getLong(0),cursor.getString(1),cursor.getDouble(2),cursor.getString(3));
            newTicket.SetChecked(cursor.getInt(4)==1);
            ticketList.add(newTicket);
        }
        return ticketList.toArray(new Ticket[0]);
    }
    public void AddManifest(String name, int id, Ticket[] tickets)
    {
        String sql = "" +
                "INSERT INTO Manifest" +
                "(ManifestId, Name)" +
                "VALUES" +
                "(?, ?);";
        getWritableDatabase().execSQL(sql, new Object[]{id,name});
        for(Ticket ticket:tickets)
        {
            sql ="INSERT INTO Ticket" +
                 "(ManifestName, TicketId, TicketType, PricePaid, OrderedBy, Checked)" +
                 "VALUES" +
                 "(?, ?, ?, ?, ?, ?);";
            getWritableDatabase().execSQL(sql,new Object[]
                    {
                        name,
                        ticket.GetTicketNumber(),
                        ticket.GetTicketType(),
                        ticket.GetPricePaid(),
                        ticket.GetOrderedBy(),
                        0
                    });
        }

    }
    public void DeleteManifest(String name)
    {
        String sql = "" +
                "DELETE FROM Ticket " +
                "WHERE ManifestName = ? ";
        getWritableDatabase().execSQL(sql,new Object[]{name});
        sql = "" +
                "DELETE FROM Manifest " +
                "WHERE Name = ?";
        getWritableDatabase().execSQL(sql,new Object[]{name});
    }

}