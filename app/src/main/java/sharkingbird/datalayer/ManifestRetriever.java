package sharkingbird.datalayer;
import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import sharkingbird.entities.Ticket;
import sharkingbird.results.ManifestRetrievalResult;

public class ManifestRetriever
{
    private int _manifestId;
    public ManifestRetriever(int manifestId)
    {
        _manifestId=manifestId;
    }
    public ManifestRetrievalResult GetTickets()
    {
        String html;
        try
        {
            html = GetHtml();
        }
        catch(IOException e)
        {
            return new ManifestRetrievalResult().new Error(e);
        }
        return new ManifestRetrievalResult().new Success(BuildTickets(html));
    }
    private Ticket[] BuildTickets(String html)
    {
        Document doc = Jsoup.parse(html);
        Elements rows = doc.select("table[border=\"1\"] tbody tr");
        ArrayList<Ticket> tickets = new ArrayList<>();
        for(Element row : rows)
        {
            Elements data = row.select(".black_vsmall");
            long ticketNum;
            try {
                ticketNum = Long.parseLong(data.get(1).text().trim());
            } catch(NumberFormatException e)
            {
                continue;
            }
            String ticketType = data.get(2).text();
            double pricePaid = Double.parseDouble(data.get(3).text().substring(0,data.get(3).text().length()-4));
            String orderedBy = data.get(4).text();
            tickets.add(new Ticket(ticketNum,ticketType,pricePaid,orderedBy));
        }
        return tickets.toArray(new Ticket[0]);
    }
    private String GetHtml() throws IOException
    {
        URL manifestUrl = new URL("http://tickets.sendomatic.com/ticketSales/manifest.php?e="+_manifestId);
        InputStreamReader reader = new InputStreamReader(manifestUrl.openConnection().getInputStream());
        StringBuilder stringBuilder = new StringBuilder();
        int data = reader.read();
        while(data!=-1)
        {
            stringBuilder.append((char)data);
            data = reader.read();
        }
        return stringBuilder.toString();
    }
}